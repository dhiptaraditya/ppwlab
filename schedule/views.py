from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import JadwalPribadi
from .forms import JadwalPribadiForms
# Create your views here.

def index(request):
  # context = PostForm()
  return render(request, 'header.html')

def addSchedule(request):
  if request.method == 'POST':
    form = JadwalPribadiForms(request.POST)
    if form.is_valid():
      form.save()
      return HttpResponseRedirect('show')
      # pass
  else:
    form = JadwalPribadiForms()
  return render(request, 'addSchedule.html', {'form' : form})
  
def seeSchedule(request):    
  tmp = JadwalPribadi.objects.all()
  return render(request, 'seeSchedule.html', {'table': tmp})

def delete(request):
  JadwalPribadi.objects.all().delete()
  form = JadwalPribadiForms(request.POST)
  return render(request, 'addSchedule.html', {'form' : form})