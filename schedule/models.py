from django.db import models
from datetime import date

# Create your models here.
class JadwalPribadi(models.Model):
  event = models.CharField( max_length = 50)
  date = models.DateTimeField()
  category = models.CharField( max_length = 30)
  place = models.CharField( max_length = 50)
  

  def __str__(self):
    return self.event