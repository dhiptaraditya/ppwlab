from django import forms
from .models import JadwalPribadi

class JadwalPribadiForms(forms.ModelForm):

  class Meta:
    model = JadwalPribadi

    fields = ('event', 'date', 'category', 'place')

    widgets = {
    'date' : forms.DateInput(attrs={'class': 'form-control',
                                'type' : 'date'}),
    'event' : forms.TextInput(attrs={'class': 'form-control',
                                'type' : 'text'}),
    'place' : forms.TextInput(attrs={'class' : 'form-control',
                                'type' : 'text'}),
    'category' : forms.TextInput(attrs={'class': 'form-control'})
    }

