from django.urls import re_path
from schedule import views
from .views import addSchedule, seeSchedule

urlpatterns = [
    re_path(r'^delete', views.delete, name='deleteSchedule'),
    re_path(r'^show', views.seeSchedule, name='SeeSchedule'),
    re_path(r'^create', views.addSchedule, name='AddSchedule'),
    re_path(r'^', views.addSchedule, name = 'navbar'),
]
