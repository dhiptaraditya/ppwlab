from django.shortcuts import render

# Create your views here.
def index(request):
	return render(request, 'Home.html')

def achievements(request):
  return render(request, 'Achievements.html')


def experiences(request):
  return render(request, 'Experiences.html')

def education(request):
  return render(request, 'Education.html')


def login(request):
  return render(request, 'Login.html')


def contactMe(request):
  return render(request, 'ContactMe.html')