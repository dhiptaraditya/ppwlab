from django.urls import re_path
from homepage import views
# from views import index
#url for app
urlpatterns = [
    re_path(r'^experiences', views.experiences, name='experiences'),
    re_path(r'^education', views.education, name='education'),
    re_path(r'^login', views.login, name='login'),
    re_path(r'^contactMe', views.contactMe, name='contactMe'),
    re_path(r'^achievements', views.achievements, name='achievements'),
    re_path(r'^', views.index, name='home'),
]
